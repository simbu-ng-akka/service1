# STAGE 1 : BUILD #####
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS Build_stage

# Set App folder as the working directory in container.
WORKDIR /App

# Copy all files inside App folder of container.
COPY . .

# Restore packages
RUN dotnet restore

# Run publish command in App folder.
# Published files will be in App/out folder.
RUN dotnet publish -c release -o out

# Install Petabridge.Cmd client
RUN dotnet tool install --global pbm 

# STAGE : RUN #####
FROM mcr.microsoft.com/dotnet/runtime:5.0 AS Run_Stage
WORKDIR /App

# Copy all files from build stage.
COPY --from=Build_Stage /App/out/. .

# Open ports
# 9110 - Petabridge.Cmd
# 6055 - Akka.Cluster. Must match to port in ap p.conf file.
# EXPOSE 9110 6055

# Run console app
ENTRYPOINT ["dotnet", "Akka.Service1.dll"]
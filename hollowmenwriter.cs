
using System;
using System.Linq;
using System.Collections.Generic;

using Akka.Actor;
using Akka.Event;

namespace Service1.Actors
{
    /// <summary>The micro service entry point actor for external messages</summary>
    public class HollowMenWriter : ReceiveActor
    {
    	private int cursor;

    	private List<string> poem;

        public HollowMenWriter()
        {
            Receive<string>(message =>
            {
                if (message.ToUpperInvariant() == "WRITEPOEM")
                {
                	WriteLineFirstLine();    	
                } 
                else 
                {
                	WriteNextLine();
                }	

                Context.System.Scheduler.ScheduleTellOnce(TimeSpan.FromSeconds(2), Context.Self, "NEXTLINE", Context.Self);
            });
      	}

      	public void WriteLineFirstLine()
        {
        	poem = Poems.HollowMen;
        	cursor = 0;
        	Context.GetLogger().Info("Writing T.S.Eliots Poem. {LineCount} Lines.", poem.Count);
        }

        public void WriteNextLine()
        {
        	cursor = ++cursor % poem.Count;
        	var line = poem.ElementAt(cursor);

        	// Warn on new verse
        	if (IsNewVerse(line)){
        		Context.GetLogger().Warning("   {LineNumber}: {Line}", cursor.ToString("000"), line);
        		return;
        	}

        	// Only output empty lines when debug logging is on.
        	if (string.IsNullOrWhiteSpace(line)){
        		Context.GetLogger().Debug("   {LineNumber}: {Line}", cursor.ToString("000"), line);
        		return;
        	}

        	Context.GetLogger().Info("   {LineNumber}: {Line}", cursor.ToString("000"), line);
        }

        public bool IsNewVerse(string line){
        	var newVerses = new List<string>{
        		"                        I",
        		"                              II",
        		"                    III",
        		"                      IV",
        		"                            V"
        	};

        	return newVerses.Where(x => x.Equals(line)).Count() > 0;
        }
    }

  	public static class Poems
  	{
  		public static List<string> HollowMen =>
  			new List<string> 
  			{
  				"",
				"The Hollow Men",
				"https://allpoetry.com/the-hollow-men",
				"",
				"Mistah Kurtz-he dead",
				"            A penny for the Old Guy",
				"",
				"",
				"                        I",
				"",
				"    We are the hollow men",
				"    We are the stuffed men",
				"    Leaning together",
				"    Headpiece filled with straw. Alas!",
				"    Our dried voices, when",
				"    We whisper together",
				"    Are quiet and meaningless",
				"    As wind in dry grass",
				"    Or rats' feet over broken glass",
				"    In our dry cellar",
				"    ",
				"    Shape without form, shade without colour,",
				"    Paralysed force, gesture without motion;",
				"    ",
				"    Those who have crossed",
				"    With direct eyes, to death's other Kingdom",
				"    Remember us-if at all-not as lost",
				"    Violent souls, but only",
				"    As the hollow men",
				"    The stuffed men.",
				"",
				"    ",
				"                              II",
				"",
				"    Eyes I dare not meet in dreams",
				"    In death's dream kingdom",
				"    These do not appear:",
				"    There, the eyes are",
				"    Sunlight on a broken column",
				"    There, is a tree swinging",
				"    And voices are",
				"    In the wind's singing",
				"    More distant and more solemn",
				"    Than a fading star.",
				"    ",
				"    Let me be no nearer",
				"    In death's dream kingdom",
				"    Let me also wear",
				"    Such deliberate disguises",
				"    Rat's coat, crowskin, crossed staves",
				"    In a field",
				"    Behaving as the wind behaves",
				"    No nearer-",
				"    ",
				"    Not that final meeting",
				"    In the twilight kingdom",
				"",
				"    ",
				"                    III",
				"",
				"    This is the dead land",
				"    This is cactus land",
				"    Here the stone images",
				"    Are raised, here they receive",
				"    The supplication of a dead man's hand",
				"    Under the twinkle of a fading star.",
				"    ",
				"    Is it like this",
				"    In death's other kingdom",
				"    Waking alone",
				"    At the hour when we are",
				"    Trembling with tenderness",
				"    Lips that would kiss",
				"    Form prayers to broken stone.",
				"",
				"    ",
				"                      IV",
				"",
				"    The eyes are not here",
				"    There are no eyes here",
				"    In this valley of dying stars",
				"    In this hollow valley",
				"    This broken jaw of our lost kingdoms",
				"    ",
				"    In this last of meeting places",
				"    We grope together",
				"    And avoid speech",
				"    Gathered on this beach of the tumid river",
				"    ",
				"    Sightless, unless",
				"    The eyes reappear",
				"    As the perpetual star",
				"    Multifoliate rose",
				"    Of death's twilight kingdom",
				"    The hope only",
				"    Of empty men.",
				"",
				"    ",
				"                            V",
				"",
				"    Here we go round the prickly pear",
				"    Prickly pear prickly pear",
				"    Here we go round the prickly pear",
				"    At five o'clock in the morning.",
				"    ",
				"    Between the idea",
				"    And the reality",
				"    Between the motion",
				"    And the act",
				"    Falls the Shadow",
				"                                    For Thine is the Kingdom",
				"    ",
				"    Between the conception",
				"    And the creation",
				"    Between the emotion",
				"    And the response",
				"    Falls the Shadow",
				"                                    Life is very long",
				"    ",
				"    Between the desire",
				"    And the spasm",
				"    Between the potency",
				"    And the existence",
				"    Between the essence",
				"    And the descent",
				"    Falls the Shadow",
				"                                    For Thine is the Kingdom",
				"    ",
				"    For Thine is",
				"    Life is",
				"    For Thine is the",
				"    ",
				"    This is the way the world ends",
				"    This is the way the world ends",
				"    This is the way the world ends",
				"    Not with a bang but a whimper.",
				"",
				"1. Mistah Kurtz: a character in Joseph Conrad's 'Heart of Darkness.'",
				"2. A...Old Guy: a cry of English children on the streets on Guy Fawkes Day, November 5, when they carry straw effigies of Guy Fawkes and beg for money for fireworks to celebrate the day. Fawkes was a traitor who attempted with conspirators to blow up both houses of Parliament in 1605; the 'gunpowder plot' failed.",
				"3. Those...Kingdom: Those who have represented something positive and direct are blessed in Paradise. The reference is to Dante's 'Paradiso'. ",
				"4. Eyes: eyes of those in eternity who had faith and confidence and were a force that acted and were not paralyzed.",
				"5. crossed stave: refers to scarecrows",
				"6. tumid river: swollen river. The River Acheron in Hell in Dante's 'Inferno'. The damned must cross this river to get to the land of the dead.",
				"7. Multifoliate rose: in dante's 'Divine Comedy' paradise is described as a rose of many leaves.",
				"8. prickly pear: cactus",
				"9. Between...act: a reference to 'Julius Caesar' 'Between the acting of a dreadful thing/And the first motion, all the interim is/Like a phantasma or a hideous dream.'",
				"10. For...Kingdom: the beginning of the closing words of the Lord's Prayer. © by owner. provided at no charge for educational purposes"
  			};
  		
  	}
}




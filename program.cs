using System;
using System.IO;

using Serilog;

using Akka.Actor;
using Akka.Configuration;

using Petabridge.Cmd.Cluster;
using Petabridge.Cmd.Cluster.Sharding;
using Petabridge.Cmd.Host;
using Petabridge.Cmd.Remote;

namespace Service1.Service
{
    class Program
    {
        static ActorSystem actorSystem;

        static int Main(string[] args)
        {
            ConfigureSeqLogging();

            CreateActorSystem();

            actorSystem.ActorOf(Props.Create(() => 
                new Service1.Actors.HollowMenWriter()), "PoemWriter")
                    .Tell("WRITEPOEM");
            
            StartPbm();

            return SetupShutdown();
        }

        static void ConfigureSeqLogging()
        {
            var logger = new Serilog.LoggerConfiguration()
                .WriteTo.Console()
                //.WriteTo.ColoredConsole()
                .WriteTo.Seq("http://seq:5341/")
                .MinimumLevel.Debug()
                .CreateLogger();
            Log.Logger = logger;
            // Only required for self diagnose of event logging issues.
            // https://medium.com/it-dead-inside/docker-containers-and-localhost-cannot-assign-requested-address-6ac7bc0d042b
            Serilog.Debugging.SelfLog.Enable(Console.Error);
            // Seq won't be running in time to log this.
            // Log.Information("Hello, {Name}!", "Service1");
        }

        static void CreateActorSystem()
        {
            // Get Hocon config
            var config = File.ReadAllText("app.conf");
            var conf = ConfigurationFactory.ParseString(config);
            // Create Actor System and create 1 API actor
            actorSystem = ActorSystem.Create("AkkaDemo", conf);
            //actorSystem.ActorOf(Props.Create(() => new Service1.Actors.Api()), "api");
        }

        static void StartPbm(){
            // Start Petabridge.Cmd (for external monitoring / supervision)
            var pbm = PetabridgeCmd.Get(actorSystem);
            pbm.RegisterCommandPalette(ClusterCommands.Instance);
            pbm.RegisterCommandPalette(ClusterShardingCommands.Instance);
            pbm.RegisterCommandPalette(RemoteCommands.Instance);
            pbm.Start();
        }

        static int SetupShutdown(){
            actorSystem.WhenTerminated.Wait();
            Log.CloseAndFlush();            
            return 0;
        }
    }
}
using Akka.Actor;

namespace Service1.Actors
{
    /// <summary>The micro service entry point actor for external messages</summary>
    public class Api : ReceiveActor
    {
        public Api()
        {
            Receive<string>(message =>
            {
                if (message.ToUpperInvariant() == "PING")
                {
                    Sender.Tell("Pong");
                }
            });
      }
    }
}
